# Write a Python program that prints the table from 1 to 12. Each row should display 4 tables with proper 
# format. 


for i in range(1,13,4):
    for j in range(1,11):

        print(f"{i:2} X {j:2} = {i*j:3}    {(i+1):2} X {j:2} = {(i+1)*j:3}    {(i+2):2} X {j:2} = {(i+2)*j:3}   {(i+3):2} X {j:2} = {(i+3)*j:3}")
    
    
    for i in range(0,3):print(" ")
