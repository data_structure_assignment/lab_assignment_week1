# Write a program in Python Sharp to display the multiplication table of a given integer. 
# Your program should look as follows: 
# Enter an Integer number (Table to be calculated): 15
#  15 X 1 = 15
# ...
# ...
# ...
# 15 X 10 = 150


num1=int(input('Enter an Integer number (Table to be calculated): '))


for i in range(1,11):
    print(str(num1)+' X '+str("%02d"%i)+' = '+str(num1*i))