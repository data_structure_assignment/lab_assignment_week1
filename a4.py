# Write a Python program that takes distance and time as input and displays the speed in, meters per 
# second, kilometres per hour and miles per hour.
# Your program should look as follows: 
# Input distance (meters): 50000
# Input time (hour): 1
# Input time(minutes): 35
# Input time(seconds): 56
#  Your speed in meters/sec is 8.686588
#  Your speed in km/h is 31.27172
#  Your speed in miles/h is 19.4355


dist_meter=int(input('Input distance (meters) : '))
time_hour=int(input('Input time (hour): '))
time_min=int(input('Input time(minutes): '))
time_sec=int(input('Input time(seconds): '))

dist_km=dist_meter*0.001
dist_mile=dist_meter *0.000621371

total_time_sec=time_hour*3600+time_min*60+time_sec
total_time_hour= time_hour+time_min/60+time_sec/3600

# print(dist_km)
# print(dist_mile)
# print(total_time_hour)
# print(total_time_sec)


print("Your speed in meters/sec is :"+ str(dist_meter/total_time_sec))
print("Your speed in km/h is  :"+ str(dist_km/total_time_hour))
print("Your speed in miles/h is  :"+ str(dist_mile/total_time_hour))
